<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.partials.head')
    @yield('head-addon')
</head>
<body>
    <div id="app">
        @include('layouts.partials.nav')

        @yield('content')
    </div>

    <!-- Scripts -->
    @include('layouts.partials.scripts')
</body>
</html>
